#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np
import pandas as pd

def stringify(x):
    if x < 10:
        return '0' + str(x)
    else:
        return str(x)

#Create simple way to read all files one by one
folder = '20190801rtfuelmix_csv'
mon = '201908'
end = 'rtfuelmix.csv'

myFile = pd.read_csv('{}/{}01{}'.format(folder, mon, end))

#All time stamps are in EDT so we don't need that column
a = myFile.pop('Time Zone')

#Data are stacked vertically, I want to reshape so that each timestamp is a row and each fuel type is a column
b = myFile.pivot(index = 'Time Stamp', columns = 'Fuel Category', values = 'Gen MW')

for i in range(2,32):
    myFile = pd.read_csv('{}/{}{}{}'.format(folder, mon, stringify(i), end))
    a = myFile.pop('Time Zone')
    today = myFile.pivot(index = 'Time Stamp', columns = 'Fuel Category', values = 'Gen MW')
    b = b.append(today, ignore_index = False)
    
b.to_csv('copy_first_concat.csv')
copy = pd.read_csv('copy_first_concat.csv')

#I want to be able to access day for grouping. also hour...
day = []
hour = []

for i in range(len(copy['Time Stamp'])):
    day.append(int(copy['Time Stamp'][i].split(' ')[0].split('/')[1]))
    time = copy['Time Stamp'][i].split(' ')[1].split(':')
    hours = int(time[0])
    mins = int(time[1])
    secs = int(time[2])
    num = hours + mins/60 + secs/3600
    hour.append(num)
    
copy['day'] = day
copy['hour'] = hour

#cool. We need emissions as well.
emfactor = dict({'Dual Fuel': 0.444, 'Natural Gas': 0.426, 'Other Fossil Fuels': 0.935, 'Other Renewables': 0.256,
                'Nuclear': 0., 'Wind': 0., 'Hydro': 0})

#new dataframe for overall emissions
em_df = pd.DataFrame(index = np.arange(1,32), columns = ['Dual Fuel kWh', 'Dual Fuel Emissions','Natural Gas kWh','Natural Gas Emissions',
                                      'Other Fossil Fuels kWh','Other Fossil Fuels Emissions',
                                       'Other Renewables kWh','Other Renewables Emissions','Total Emissions'])


for i in range(1,32):
    today = copy[copy['day'] == i]
    times = today['hour']
    fuel = today['Dual Fuel']
    gas = today['Natural Gas']
    fossil = today['Other Fossil Fuels']
    rens = today['Other Renewables']
    
    #mWh to kWh is *1000
    totalFuel = np.trapz(fuel, x = times) * 1000
    totalGas = np.trapz(gas, x = times) * 1000
    totalFossil = np.trapz(fossil, x = times) * 1000
    totalRens = np.trapz(rens, x = times) * 1000
    
    #Multiply by their emission factor
    fuel_em = totalFuel * emfactor['Dual Fuel']
    gas_em = totalGas * emfactor['Natural Gas']
    fos_em = totalFossil * emfactor['Other Fossil Fuels']
    rens_em = totalRens * emfactor['Other Renewables']
    
    total_em = fuel_em + gas_em + fos_em + rens_em
    
    em_df.loc[i] = [totalFuel, fuel_em, totalGas, gas_em, totalFossil, fos_em, totalRens, rens_em, total_em]
    
def report_day(x = int(input('Please enter a day to summarize CO2 emissions (between 1 and 31)')), file = em_df):
    day = file.iloc[x-1]
    print("Selected Day: August {}".format(x))
    print("KiloWatt-hours of Dual Fuel: {} kWh".format(day['Dual Fuel kWh']))
    print("Associated emissions: {} kg CO2 emitted".format(day['Dual Fuel Emissions']))
    print()
    print("KiloWatt-hours of Natural Gas: {} kWh".format(day['Natural Gas kWh']))
    print("Associated emissions: {} kg CO2 emitted".format(day['Natural Gas Emissions']))
    print()
    print("KiloWatt-hours of Other Fossil Fuels: {} kWh".format(day['Other Fossil Fuels kWh']))
    print("Associated emissions: {} kg CO2 emitted".format(day['Other Fossil Fuels Emissions']))
    print()
    print("KiloWatt-hours of Other Renewables: {} kWh".format(day['Other Renewables kWh']))
    print("Associated emissions: {} kg CO2 emitted".format(day['Other Renewables Emissions']))
    print()
    print("Total emissions for August {}: {} kg CO2 emitted".format(x, day['Total Emissions']))
    
print(report_day())

