##### HOW TO RUN THE MODEL #####

Open end_to_end.py in a compatible application (Python 3.9)

You will be prompted to enter an integer representing the date of data that you would like to see.
Type the date and press enter.

Data will be displayed and the script will end.


Dependencies:
All csv files in folder 20190801rtfuelmix_csv must not be altered.

Package Versions:
matplotlib v 3.1.3
numpy v 1.19.3
pandas v 1.0.3

